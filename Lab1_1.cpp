# include <iostream>
#include <vector>
#include <iterator>
using namespace std;

const int MIN_ROW_COUNT = 5;
const int MAX_ROW_COUNT = 30;

const int MIN_COLUMN_COUNT = 5;
const int MAX_COLUMN_COUNT = 30;

const int MIN_MINE_COUNT = 1;
const int MAX_MINE_COUNT = 800;

class MinesweeperCell //класс
{
private:
    int row;
    int column;
    string state = {"closed"};
    bool mined;
    int counter;


public:
    MinesweeperCell(int Row, int Column) //эти параметры мы передадим при создании объекта в main
    {
        row = Row;
        column = Column;
        state = {"closed"};
        mined = 0;
        counter = 0;
        cout << "Ячейка готова!"; //ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
};

class MinesweeperModel
{
private:
    int rowCount;
    int columnCount;
    int mineCount;
public:
    MinesweeperModel(int RowCount = 15, int ColumnCount = 15, int MineCount = 15)
    {
        if(MIN_ROW_COUNT < rowCount < MAX_ROW_COUNT + 1) rowCount = RowCount;

        if(MIN_COLUMN_COUNT < columnCount < MAX_COLUMN_COUNT + 1) columnCount = ColumnCount;

        if(MIN_MINE_COUNT < rowCount < MAX_MINE_COUNT + 1) mineCount = MineCount;

        else mineCount = rowCount * columnCount - 1;

        bool firstStep = 1;
        bool gameOver = 0;
        vector<MinesweeperCell> cellsTable;

        for (int row = 0;row < rowCount;++row)
        {
            vector<MinesweeperCell> cellsRow;
            for (int column = 0; column < columnCount; ++column)
            {
                cellsRow.insert(cellsRow.end(),(MinesweeperCell(row, column)));
            }
            cellsTable.insert(cellsTable.end(), cellsRow.begin(), cellsRow.end());
            cellsRow.clear();
        }
        cout << endl << "Строчки: " << rowCount << " Колонки: " << columnCount << " Мины: " <<mineCount;//ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        cout << endl << "Количество ячеек: " << cellsTable.size();//ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    }
};

int main()
{
    MinesweeperCell obj1(3, 6);
    cout << endl;
    MinesweeperModel obj2(6, 8, 12);

    return 0;
}
